/* Copied from pgadmin */
CREATE DATABASE hw_10
    WITH
    OWNER = db_user
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
/* Copied from pgadmin */

create table if not exists address(
	id serial primary key,
	street varchar(255) not null,
	country varchar(255),
	city varchar(255),
	zip_code int
);

create table if not exists user_db(
	id serial primary key,
	first_name varchar(255) not null,
	last_name varchar(255),
	age int
);

create table if not exists location(
	id serial primary key,
	name varchar(255) not null,
	score int,
	address_id int not null,
	foreign key(address_id) references address(id) on delete cascade
);

create table if not exists user_address(
	id serial primary key,
	id_user int not null,
	id_address int not null,
	foreign key(id_user) references user_db(id) on delete cascade,
	foreign key(id_address) references address(id) on delete cascade
);


insert into user_db(first_name, last_name, age)
values ('Tim', 'Bits', 23),
       ('Mike', 'Ken', 80),
       ('Olivia', 'White', 34);

insert into address(street, country, city, zip_code)
values ('Naberejnaya', 'Ukraine', 'Energodar', 71503),
	   ('Dniprovska', 'Ukraine', 'Zaporizhzya', 45643),
	   ('Shevchenka', 'Ukraine', 'Vinnica', 43255),
	   ('Hoholya', 'Ukraine', 'Artemivsk', 32345),
	   ('Prymorsk', 'Ukraine', 'Odessa', 34657),
	   ('Schchorsa', 'Ukraine', 'Balta', 68653),
	   ('Partyzanska', 'Ukraine', 'Mykolaiv', 23456),
	   ('Polova', 'Ukraine', 'Berdyansk', 23354);

insert into location(name, score, address_id)
values ('location 1', 1, 1),('location 2', 10, 1),
	   ('location 3', 12, 3),('location 4', 21, 5),
	    ('location 5', 32, 1),('location 6', 4, 4);


insert into user_address(id_user, id_address)
values (1, 1),(1, 2),
	   (2, 1),(2, 2),
	   (3 ,4), (3, 3);


select * from user_db as ud
	left join user_address as ua on ud.id = ua.id_user
	left join address on ua.id_address = address.id

select ud.first_name, ud.last_name, ud.age, ad.street from user_db as ud
	left join user_address as ua on ud.id = ua.id_user
	left join address as ad on ua.id_address = ad.id
	where ud.age = 34

select ud.first_name, ud.age, ad.street from user_db as ud
	left join user_address as ua on ud.id = ua.id_user
	left join address as ad on ua.id_address = ad.id
	where ud.age = 34 and ad.street like '%chenka%'
